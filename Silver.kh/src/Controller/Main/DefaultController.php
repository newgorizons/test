<?php

namespace App\Controller\Main;

use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="main_homepage")
     * @param EntityManagerInterface $entityManager
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $productList = $entityManager->getRepository(Product::class)->findAll();
        //dd($productList);

        return $this->render('main/default/index.html.twig', []);
    }

//    /**
//     * @Route ("/product-add", name = "product_add_old")
//     * @param Request                $request
//     * @param EntityManagerInterface $entityManager
//     * @return Response
//     */
//    public function productAdd(
//        Request $request,
//        EntityManagerInterface $entityManager
//    ):Response
//    {
//        $product = new Product();
//        $product->setTitle('Product'.rand(1, 100));
//        $product->setDescription('BDSM');
//        $product->setPrice(10);
//        $product->setQuantity(1);
//
//        $entityManager->persist($product);
//        $entityManager->flush();
//
//        return $this->redirectToRoute('homepage');
//    }
}
