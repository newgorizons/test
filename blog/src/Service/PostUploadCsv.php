<?php

namespace App\Service;

use App\Entity\Post;

/**
 * Class PostUploadCsv
 * @author Dmitriy Privedeniy <silver.kh.2012@gmail.com>
 */
class PostUploadCsv implements PostUploadInterface
{
    /**
     * @var  string
     */
    protected $post;

    /**
     * @param Post $post
     * @return void
     */
    public function setPost(Post $post):void
    {
        $this->post = $post;
    }

    /**
     * @return string
     */
    public function getFormat()
    {
        return 'csv';
    }

    /**
     * @return string
     */
    public function upload()
    {
        return $this->post->getName().';'.$this->post->getDescription();
    }
}
