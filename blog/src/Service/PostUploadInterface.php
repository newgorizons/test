<?php
/**
 * @author Dmitriy Privedeniy <silver.kh.2012@gmail.com>
 */

namespace App\Service;

use App\Entity\Post;

interface PostUploadInterface
{
    /**
     * @param Post $post
     * @return void
     */
    public function setPost(Post $post);

    /**
     * @return string
     */
    public function getFormat();

    /**
     * @return string
     */
    public function upload();
}
