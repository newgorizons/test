<?php

namespace App\Service;

use App\Entity\Post;

/**
 * Class PostUploadHtml
 * @author Dmitriy Privedeniy <silver.kh.2012@gmail.com>
 */
class PostUploadHtml implements PostUploadInterface
{
    /**
     * @var string
     */
    protected $post;

    /**
     * @param Post $post
     * @return void
     */
    public function setPost(Post $post):void
    {
        $this->post = $post;
    }

    /**
     * @return string
     */
    public function getFormat()
    {
        return 'html';
    }

    /**
     * @return string
     */
    public function upload()
    {
        $content = '';
        $content .= '<b>' . $this->post->getName() . '</b>' . "</br>\n";
        $content .= '<p>'.$this->post->getDescription().'</p>'."</br>\n";
        return $content;
    }
}
