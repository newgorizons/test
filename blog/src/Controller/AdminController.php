<?php

namespace App\Controller;

use App\Repository\PostRepository;
use App\Repository\SubscriptionRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Post;
use App\Entity\Users;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Class AdminController
 * @IsGranted("ROLE_ADMIN")
 * @Route ("/admin")
 * @package App\Controller
 * @author  Dmitriy Privedeniy <silver.kh.2012@gmail.com>
 */
class AdminController extends AbstractController
{
    /**
     * @Route ("/", name="admin_index")
     * @param PostRepository         $postRepository
     * @param UserRepository         $userRepository
     * @param SubscriptionRepository $subscriptionRepository
     * @return Response
     */
    public function index(
        PostRepository $postRepository,
        UserRepository $userRepository,
        SubscriptionRepository $subscriptionRepository
    ): Response {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $subscribers = $subscriptionRepository->findAll();
        $user = $userRepository->findAll();
        return $this->render(
            'users/admin.html.twig',
            [
            'user' => $user,
            'subscribers' => $subscribers,
            ]
        );
    }
}
