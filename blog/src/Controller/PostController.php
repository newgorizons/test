<?php

/**
 * @author Dmitriy Privedeniy <silver.kh.2012@gmail.com>
 */

namespace App\Controller;

use App\Entity\Post;
use App\Event\PostCreatedEvent;
use App\Event\PostDeletedEvent;
use App\Form\FormPost;
use App\Service\PostUploadCsv;
use App\Service\PostUploadHtml;
use App\Service\PostUploadInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Class PostController
 * @package App\Controller
 * @Route   ("/post")
 */
class PostController extends AbstractController
{
    /**
     * метод создания и записи в базу данных поста
     * @IsGranted("ROLE_ADMIN")
     * @param  Request                  $request    Request
     * @param  LoggerInterface          $logger
     * @param  EventDispatcherInterface $dispatcher
     * @Route  ("/create", name="post_create")
     * @return Response
     */
    public function create(
        Request $request,
        LoggerInterface $logger,
        EventDispatcherInterface $dispatcher
    ): Response {
        $logger->info('Enter the title of the post');

        $post = new Post();
        $post->setName('this is the new news in the post');
        $post->setPublishedAt(new \DateTime());
        $formPost = $this->createForm(FormPost::class, $post);
        $formPost->handleRequest($request);
        if ($formPost->isSubmitted() && $formPost->isValid()) {
            $logger->info('form is valid');
            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();

            $logger->info('post saved');

            $dispatcher->dispatch(new PostCreatedEvent($post), PostCreatedEvent::NAME);

            return $this->redirectToRoute(
                'post_show',
                [
                    'post' => $post->getId(),
                ]
            );
        }
        return $this->render(
            'post/create.html.twig',
            [
                'post' => $post,
                'formPost' => $formPost->createView(),
            ]
        );
    }

    /**
     * метод редактирования поста
     * @param  Post                   $post
     * @param  Request                $request
     * @param  EntityManagerInterface $entityManager
     * @Route  ("/edit/{post}", name="post_edit")
     * @return Response
     */
    public function edit(Post $post, Request $request, EntityManagerInterface $entityManager): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $formPost = $this->createForm(FormPost::class, $post);
        $formPost->handleRequest($request);

        if ($formPost->isSubmitted() && $formPost->isValid()) {
            $entityManager->persist($post);
            $entityManager->flush();
            return $this->redirectToRoute(
                'post_show',
                [
                    'post' => $post->getId(),
                ]
            );
        }
        return $this->render(
            'post/edit.html.twig',
            [
            'post' => $post,
            'formPost' => $formPost->createView(),
            ]
        );
    }

    /**
     * метод удаления поста
     * @param Post                     $post
     * @param EntityManagerInterface   $em
     * @param EventDispatcherInterface $dispatcher
     * @param MailerInterface          $mailer
     * @return Response
     * @Route  ("/delete/{post}", name="post_delete")
     */
    public function delete(
        Post $post,
        EntityManagerInterface $em,
        EventDispatcherInterface $dispatcher,
        MailerInterface $mailer
    ) {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $dispatcher->dispatch(new PostDeletedEvent($post), PostDeletedEvent::NAME);

        $em->remove($post);
        $em->flush();
        return $this->redirectToRoute('homepage');
    }

    /**
     * метод демонстрации поста
     *
     * @param  Post $post
     * @Route  ("/show/{post}", name="post_show")
     * @return Response
     */
    public function showPost(Post $post): Response
    {
        return $this->render(
            'post/show.html.twig',
            [
                'post' => $post,
            ]
        );
    }

    /**
     * @Route ("/download/{post}.html", name="post_download_html")
     * @param Post           $post
     * @param PostUploadHtml $uploadHtml
     * @return Response
     */
    public function domnloadHtml(Post $post, PostUploadHtml $uploadHtml)
    {
        $uploadHtml->setPost($post);
        $content = $uploadHtml->upload();
        $filename = md5(uniqid()).'.'.$uploadHtml->getFormat();
        // возвращает ответ с содержимым поста
        $response = new Response($content);
        // Создаем диспозицию файла
        $disposition = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $filename
        );
        // Установите расположение содержимого.
        $response->headers->set('Content-Disposition', $disposition);
        // Диспетчерский запрос
        return $response;
    }

    /**
     * @Route ("/download/{post}.csv", name="post_download_csv")
     * @param Post          $post
     * @param PostUploadCsv $uploadCsv
     * @return Response
     */
    public function downloadCsv(Post $post, PostUploadCsv $uploadCsv)
    {
        $uploadCsv->setPost($post);
        $content = $uploadCsv->upload();
        $filename = md5(uniqid()).'.'.$uploadCsv->getFormat();
        $response = new Response($content);
        // Создаем диспозицию файла
        $disposition = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $filename
        );
        // Set the content disposition
        $response->headers->set('Content-Disposition', $disposition);
        // Dispatch request
        return $response;

    }

    /**
     * @Route ("/download/{post}", name="post_download")
     * @param Post                $post
     * @param PostUploadInterface $upload
     * @return Response
     */
    public function downloadAction(Post $post, PostUploadInterface $upload)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $upload->setPost($post);
        $content = $upload->upload();
        return new Response($content);
    }
}
