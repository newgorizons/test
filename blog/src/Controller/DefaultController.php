<?php

namespace App\Controller;

use App\Entity\Subscription;
use App\Form\FeedbackForm;
use App\Form\SubscriptionForm;
use App\Repository\PostRepository;
use App\Repository\SubscriptionRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Post;
use App\Entity\Users;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Class DefaultController
 *
 * @package App\Controller
 * @author  Dmitriy Privedeniy <silver.kh.2012@gmail.com>
 */
class DefaultController extends AbstractController
{
    /**
     * @return RedirectResponse
     */
    public function defaultLocale()
    {
        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route  ("/", name="homepage")
     * @param PostRepository $postRepository
     * @param Request        $request
     * @return Response
     */
    public function index(PostRepository $postRepository, Request $request): Response
    {
        $post = $postRepository->findAll();

        $page = $request->get('page', 0);
        $offset = $page * PostRepository::PAGINATOR_PER_PAGE;
        $list = $postRepository->getListPaginator($offset);

        $nameOne = 'Mark';
        $nameTwo = 'Diana';
        $ageMark = 12;
        $ageDiana = 5;

        $users = [
            ['name' => 'Dima','age' => 36],
            ['name' => 'Nata','age' => 39]
        ];

        return $this->render(
            'default/index.html.twig',
            [
                'nameOne' => $nameOne,
                'nameTwo' => $nameTwo,
                'ageMark' => $ageMark,
                'ageDiana' => $ageDiana,
                'users' => $users,
                'post' => $post,
                'list' => $list,
                'pager' => [
                    'pages' => floor(count($list)/PostRepository::PAGINATOR_PER_PAGE),
                    'page' => $page,
                    'prev' => $page-1,
                    'next' => $page+1,
                         ]
            ]
        );
    }

//    /**
//     * @Route("/", name="post_index")
//     * @param  Request        $request    Request
//     * @param  PostRepository $post
//     * @return \Symfony\Component\HttpFoundation\Response
//     */
//    public function indexAction(PostRepository $post, Request $request)
//    {
//        $page = $request->get('page', 0);
//        $offset = $page * PostRepository::PAGINATOR_PER_PAGE;
//        $list = $post->getListPaginator($offset);
//
//        return $this->render('default/index.html.twig', [
//            'list' => $list,
////            'pager' => [
////                'pages' => floor(count($list)/PostRepository::PAGINATOR_PER_PAGE),
////                'page' => $page,
////                'prev' => $page-1,
////                'next' => $page+1,
////            ]
//        ]);
//    }

    /**
     * @Route  ("/about", name="default_about")
     * @return Response
     */
    public function about(): Response
    {
        return $this->render('default/about.html.twig');
    }

    /**
     * @Route  ("/feedback", name="default_feedback")
     * @param Request         $request
     * @param MailerInterface $mailer
     * @return Response
     */
    public function feedback(Request $request, MailerInterface $mailer): Response
    {
        $formFeedback = $this->createForm(FeedbackForm::class);
        $formFeedback->handleRequest($request);
        if ($formFeedback->isSubmitted() && $formFeedback->isValid()) {
            $data = $formFeedback->getData();
            $message = new Email();
            $message->from('burm.courses@gmail.com');
            $message->to('Silver.kh.2012@gmail.com');
            $message->subject('Feedback response');
            $message->text('Это ответ обратной связи');
            $message->html($this->renderView(
                'email/feedback.html.twig',
                [
                    'data' => $data,
                ]
            ));
            $mailer->send($message);

        }
        return $this->render(
            'default/feedback.html.twig',
            [
            'form' => $formFeedback->createView(),
            ]
        );
    }

//    /**
//     * @Route("/search", name="search")
//     * @param Request $request
//     * @return Response
//     */
//    public function search(Request $request): Response
//    {
//        $list = $this->getDoctrine()
//            ->getRepository(Post::class)
//            ->search($request->get('keyword'), $request->getLocale());
//        return $this->render('default/search.html.twig', [
//            'list' => $list,
//        ]);
//    }

    /**
     * @Route ("/login", name="login")
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils) : Response
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render(
            'default/login.html.twig',
            [
                'last_username' => $lastUsername,
                'error'         => $error,
            ]
        );
    }
    /**
     * метод для записи в таблицу почты подписчика
     * @param  Request                $request       Request
     * @param  EntityManagerInterface $entityManager
     * @Route  ("/subscribe", name="subscribe")
     * @return Response
     */
    public function subscribe(Request $request, EntityManagerInterface $entityManager) : Response
    {
        $subscriber = new Subscription();
        $subscriber->setEmail('@');
        $subscriber->setSubscriptionDate(new \DateTime());
        $form = $this->createForm(SubscriptionForm::class, $subscriber);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($subscriber);
            $entityManager->flush();
            return $this->redirectToRoute('homepage');
        }
        return $this->render(
            'users/subscribe.html.twig',
            [
                'subscriber' => $subscriber,
                'form' => $form->createView(),
            ]
        );
    }
}
