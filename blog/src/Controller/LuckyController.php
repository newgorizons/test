<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class LuckyController extends AbstractController
{
    /**
     * Метод для конвертации доларра в гривну по фиксированому курсу 27,3
     *
     * @author Dmitriy Privedeniy <silver.kh.2012@gmail.com>
     * @Route  ("/lucky/converter", name="lucky_converter")
     * @return Response
     */
    public function converter(): Response
    {
        return $this->render('lucky/converter.html.twig');
    }
    /**
     * @author Dimon Drugoi <MarkPriv2009@gmail.com>
     * @Route  ("/lucky/in", name="lucky_index")
     * @return Response
     */
    public function index():Response
    {
        return $this->render('lucky/index.html.twig');
    }
}
