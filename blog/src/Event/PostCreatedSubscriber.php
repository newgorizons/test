<?php
namespace App\Event;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\MailerInterface;
use App\Entity\User;
use Symfony\Component\Mime\Email;
use Twig\Environment;

class PostCreatedSubscriber implements EventSubscriberInterface
{
    protected $twig;
    protected $mailer;
    protected $users;

    public function __construct(
        MailerInterface $mailer,
        EntityManagerInterface $em,
        Environment $twig
    ) {
        $this->mailer = $mailer;
        $this->em = $em;
        $this->twig = $twig;
    }
    public static function getSubscribedEvents()
    {
        return [
            PostCreatedEvent::NAME => [
                ['sendEmailAdmin', 10],
            ],
        ];
    }
    public function sendEmailAdmin(PostCreatedEvent $createdEvent)
    {
        $users = $this->em->getRepository(User::class)->findAll();
        $post = $createdEvent->getPost();
        foreach ($users as $user) {
            $email = $user->getEmail();// "достучались" до адреса почты каждого админа
            $message = new Email();
            $message->from('burm.courses@gmail.com');
            $message->to($email);
            $message->subject('Создание поста');
            $message->text(
                'Ув.Амин на ресурсе Developer. Launch Symfony PHP был создан пост,его ID = '
                .$post->getId().'. Имя поста: '.$post->getName().
                '. Вот его содержимое: '.$post->getDescription()
            );
            $this->mailer->send($message);
        }

    }
}
