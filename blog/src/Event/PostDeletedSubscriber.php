<?php
namespace App\Event;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Twig\Environment;

class PostDeletedSubscriber implements EventSubscriberInterface
{
    protected $twig;
    protected $mailer;
    public function __construct(
        MailerInterface $mailer,
        Environment $twig
    ) {
        $this->mailer = $mailer;
        $this->twig = $twig;
    }

    public static function getSubscribedEvents()
    {
        return [
                    PostDeletedEvent::NAME => [
                        ['sendEmail', 10],
                    ],
        ];
    }
    public function sendEmail(PostDeletedEvent $deletedEvent)
    {
         $post = $deletedEvent->getPost();
  //      echo 'YO BRO is sending Email#'.$post->getId().'</br>';
          $message = new Email();
          $message->from('burm.courses@gmail.com');
          $message->to('Silver.kh.2012@gmail.com');
          $message->subject('Удаление поста');
        $message->text(
            'Если еще и на уроке по отправке почты застряну - нажрусь!Кстати пост '
              . $post->getId().' был удален'
        );
        $message->html($this->twig->render(
            'email/delete.html.twig',
            [
                  'post' => $post,
              ]
        ));
          $this->mailer->send($message);
    }
}
