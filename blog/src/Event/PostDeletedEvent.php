<?php
namespace App\Event;

use Symfony\Contracts\EventDispatcher\Event;
use App\Entity\Post;

class PostDeletedEvent extends Event
{
    const NAME = 'post.deleted';
    protected $post;
    public function __construct(Post $post)
    {
        $this->post = $post;
    }
    public function getPost()
    {
        return $this->post;
    }
}
