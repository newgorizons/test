<?php
namespace App\Event;

use Symfony\Contracts\EventDispatcher\Event;
use App\Entity\Post;

class PostEditEvent extends Event
{
    const NAME = 'post.edit';
    protected $post;
    public function __construct(Post $post)
    {
        $this->post = $post;
    }
    public function getPost()
    {
        return $this->post;
    }
}
