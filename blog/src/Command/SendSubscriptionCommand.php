<?php

/**
 * @author Dmitriy Privedeniy <silver.kh.2012@gmail.com>
 */

namespace App\Command;

use App\Entity\Post;
use App\Entity\Subscription;
use App\Form\SubscriptionForm;
use App\Form\UserForm;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Repository\SubscriptionRepository;
use App\Repository\PostRepository;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Validator\Constraints\DateTime;
use Twig\Environment;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class SendSubscriptionCommand
 * @package App\Command
 */
class SendSubscriptionCommand extends Command
{
    protected static $defaultName = 'send-subscription';
    protected static $defaultDescription = 'консольная команда для рассылки подписчикам новых постов за кол-во дней указаных в аргументе';

    protected $mailer;
    protected $em;
    protected $twig;
    protected $router;

    public function __construct(
        MailerInterface $mailer,
        EntityManagerInterface $em,
        Environment $twig,
        RouterInterface $router
    )
    {
        $this->mailer = $mailer;
        $this->em = $em;
        $this->twig = $twig;
        $this->router = $router;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('a', InputArgument::REQUIRED, 'how many days to sample the posts');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $a = $input->getArgument('a');

         $posts = $this->em->getRepository(Post::class)->findAll();// предопределили переменной $posts все посты
         $subscribers = $this->em->getRepository(Subscription::class)->findAll();//присвоиили переменной сущность подписчика

         $datenow = new \DateTime();// текущая время и дата
        // c помощью аргумента указываем за сколько дней нам необходимо сделать выборку постов от текущего времени
         $minusDays = $datenow->modify('-'.$a.' days');

         $io = new SymfonyStyle($input, $output);
         $io->progressStart(count($subscribers)); // добовляем прогресс бар

        foreach ($subscribers as $subscriber) {
            $io->progressAdvance();
            $email = $subscriber->getEmail(); // "достучались" до адреса почты каждого подписчика

            foreach ($posts as $post) {
                $namePost = $post->getName(); // извлекли имя поста
                $dateLastPost = $post->getPublishedAt(); // извлекли дату создания всех постов
                $datePost = $post->getPublishedAt()->format('Y-m-d');  // Привели дату в строку

//todo необходима проработка сравнения дат на уровне класса DateTime.
//                $diff = $datenow->diff($dateLastPost);
//                //$diff = $dateLastPost->diff($minusDays);
//                //echo $diff->format('%R%a дней');
//todo доработка создания абсолютной ссылки для отправки в письме
//                $context = $this->router->getContext();
//                $context->setHost('localhost');
//                $context->setScheme('https');
//                $context->setBaseUrl('my/path');
//                $url = $this->router->generate('post_show', ['id' => 5, 'locale' => 'ru']);
                //Бурмистров Александр, [26.11.2021 10:36]
                //тут равзве что язык еще нужен,
                // поскольку в веб-е берется текущий выбранный язык,
                // а в консоли его нет и его надо передать принудительно
//                var_dump($url);die();

                if ($dateLastPost > $minusDays) {
                    $message = new Email();
                    $message->from('burm.courses@gmail.com');
                    $message->to($email);
                    $message->subject('Subscription mailing');
                    $message->text('Уважаемый подписчик, на ресурсе Developer. Launch Symfony PHP появились новые посты : '
                    .$namePost.' от '.$datePost);
                    $message->html($this->twig->render(
                        'Email/subscribers.html.twig',
                        [
                           'namePost' => $namePost,
                           'datePost' => $datePost,
                        ]
                    ));
                    $this->mailer->send($message);
                }
            }
        }
        $io->progressFinish();
        $io->success('Weekly reports were sent to subscribers!');
        return 0;
        return Command::SUCCESS;
    }
}
