<?php

namespace App\Repository;

use App\Entity\Post;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Post|null find($id, $lockMode = null, $lockVersion = null)
 * @method Post|null findOneBy(array $criteria, array $orderBy = null)
 * @method Post[]    findAll()
 * @method Post[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostRepository extends ServiceEntityRepository
{
    public const PAGINATOR_PER_PAGE = 5;
    /**
     * PostRepository constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Post::class);
    }

    public function getListPaginator(int $offset): Paginator
    {
        $query = $this->createQueryBuilder('c')
            ->setMaxResults(self::PAGINATOR_PER_PAGE) // LIMIT 5
            ->setFirstResult($offset) // LIMIT 0, 5
            ->getQuery()
        ;
        return new Paginator($query);
    }

//    public function search($k, $locale)
//    {
//        $conn = $this->getEntityManager()->getConnection();
//
//        $sql = '
//            SELECT
//                post.id,
//                post.published_at,
//                post.name,
//                post.description
//            FROM post
//            WHERE
//                post.locale = :locale
//                AND
//                (post.name LIKE :keyword OR post.description LIKE :keyword)
//            ORDER BY
//                 post.published_at DESC
//            ';
//        $stmt = $conn->prepare($sql);
//        $stmt->execute([
//            'locale' => $locale,
//            'keyword' => '%' . $k . '%'
//        ]);
//
//        // returns an array of arrays (i.e. a raw data set)
//        return $stmt->fetchAllAssociative();
//    }


    // /**
    //  * @return Post[] Returns an array of Post objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Post
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
