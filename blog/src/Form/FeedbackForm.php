<?php

namespace App\Form;

use phpDocumentor\Reflection\Types\Void_;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class FeedbackForm
 * @package App\Form
 */
class FeedbackForm extends AbstractType
{
    /**
     * @param  FormBuilderInterface $builder
     * @param  array                $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options) :void
    {
        $builder->add('name', TextType::class);
        $builder->add('contacts', TextType::class);
        $builder->add('description', TextType::class);
    }
}
