<?php

namespace App\Tests;

use App\Entity\Post;
use App\Entity\Users;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PostEntityTest extends WebTestCase
{
    public function testSetGetPost(): void
    {
        $post = new Post();
        $name = 'New post for test';
        $post->setName($name);
        $this->assertTrue($post->getName() == $name );
        $description = 'A short description for the test';
        $post->setDescription($description);
        $this->assertTrue($post->getDescription() == $description);
    }
    public function testSetGetUser(): void
    {
        $user = new Users();
        $name = 'Dima';
        $user->setName($name);
        $this->assertTrue($user->getName() == $name );
        $mail = 'silver.kh.2012@gmail.com';
        $user->setMail($mail);
        $this->assertTrue($user->getMail() == $mail);
        $password = 1234554321;
        $user->setPassword($password);
        $this->assertTrue($user->getPassword() == $password);

    }
}
