<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Repository\PostRepository;

class PostControllerTest extends WebTestCase
{
//    protected function getLastId()
//    {
//        $postRepositoriy = static ::getContainer()->get(PostRepository::class);
//        $post = $postRepositoriy->findOneBy([], ['id'=>'desc']);
//        return $post->getId();
//    }
    public function testPostCreate(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');
        $postCreateLinc = $crawler->selectLink('New post');
        $this->assertCount(1,$postCreateLinc);
        $crawler = $client->request('GET', '/post/create');
        $this->assertSelectorTextContains('h4', 'Сreate a new post');
        $this->assertResponseIsSuccessful();
    }
   // public function testCreatePost()
   // {
        //$client = static::createClient();
        //$toLastId = $this->getLastId();

//        $crawler = $client->request('GET', '/post/create');
//        $this->assertResponseIsSuccessful();
//
//        $form = $crawler->selectButton('Submit')->form();
//        $form['post_form[name]'] = 'Post from phpUnit';
//        $form['post_form[description]'] = 'large herbivore';
//        $client->submit($form);

        //$afterLastId = $this->getLastId();

        //$this->assertTrue($afterLastId > $toLastId);
        //$this->assertResponseRedirects('/post/show/' . $afterLastId);
   // }

//    public function testPostDelete(): void
//    {
//        $client = static::createClient();
//        $client->request('GET', '/post/delete/' . $this->getLastId());
//        $this->assertResponseRedirects('/');
//    }

}
