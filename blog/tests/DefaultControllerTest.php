<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testLinkPageCheck(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Developer Launch PHP');
        $AboutLinc = $crawler->selectLink('About');
        $this->assertCount(1,$AboutLinc);
        $crawler = $client->request('GET', '/about');
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h4', 'About');
        $FeedbackLinc = $crawler->selectLink('Feedback');
        $this->assertCount(1,$FeedbackLinc);
  //    $crawler = $client->request('GET','/feedback');
  //    $this->assertResponseIsSuccessful();
  //    $this->assertSelectorTextContains('h4', 'Feedback page');
        $instagram = $crawler->selectLink('Instagram');
        $this->assertCount(1,$instagram);
        $facebook = $crawler->selectLink('Facebook');
        $this->assertCount(1,$facebook);
        $YouTube = $crawler->selectLink('YouTube');
        $this->assertCount(1,$YouTube);
    }
}
