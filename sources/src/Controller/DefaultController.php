<?php

namespace App\Controller;

use App\Repository\PostRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Post;
use App\Entity\Users;

/**
 * Class DefaultController
 * @package App\Controller
 * @author Dmitriy Privedeniy <silver.kh.2012@gmail.com>
 */
class DefaultController extends AbstractController
{
    /**
     * @Route ("/", name="homepage")
     * @return Response
     */
    public function index(PostRepository $postRepository): Response
    {
        $nameOne = 'Mark';
        $nameTwo = 'Diana';
        $ageMark = 12;
        $ageDiana = 5;

        $users = [
            ['name' => 'Dima','age' => 36],
            ['name' => 'Nata','age' => 39]
        ];
        $post = $postRepository->findAll();
        return $this->render(
            'default/index.html.twig',
            [
                'nameOne' => $nameOne,
                'nameTwo' => $nameTwo,
                'ageMark' => $ageMark,
                'ageDiana' => $ageDiana,
                'users' => $users,
                'post' => $post
            ]
        );
    }

    /**
     * @Route ("/about", name="default_about")
     * @return Response
     */
    public function about(): Response
    {
        return $this->render('default/about.html.twig');
    }

    /**
     * @Route ("/feedback", name="default_feedback")
     * @return Response
     */
    public function feedback(): Response
    {
        return $this->render('default/feedback.html.twig');
    }
    /**
     * @Route ("/users", name="default_users")
     * @return Response
     */
    public function createUsers(EntityManager $entityManager): Response
    {
        $user = new Users();
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
        $user->setName(substr(str_shuffle($permitted_chars), 0, 10));
        $user->setMail('Silver.kh.2012@gmail.com');
        $user->setPassword(rand(4999, 9999));
        $entityManager->persist($user);
        $entityManager->flush();
        dd($user);
        return new Response();
    }
}
