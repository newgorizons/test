<?php

/**
 * @author Dmitriy Privedeniy <silver.kh.2012@gmail.com>
 */

namespace App\Controller;

use App\Entity\Post;
use App\Form\FeedbackPost;
use App\Service\PostExporterCsv;
use App\Service\PostExporterHtml;
use App\Service\PostExporterInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PostController
 * @package App\Controller
 * @Route ("/post")
 */
class PostController extends AbstractController
{
    /**
     * метод создания и записи в базу данных поста
     * @param Request         $request Request
     * @param LoggerInterface $logger
     * @Route ("/create", name="post_create")
     * @return Response
     */
    public function create(Request $request, LoggerInterface $logger): Response
    {
        $logger->info('Run create my post method');
        $post = new Post();
        $post->setName('this is the new news in the post');
        $post->setPublishedAt(new \DateTime());
        $formPost = $this->createForm(FeedbackPost::class, $post);
        $formPost->handleRequest($request);
        if ($formPost->isSubmitted() && $formPost->isValid()) {
            $logger->info('form is valid');
            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();
            $logger->info('post saved');
            return $this->redirectToRoute(
                'post_show',
                [
                    'post' => $post->getId(),
                ]
            );

        }
        return $this->render(
            'post/create.html.twig',
            [
                'post' => $post,
                'formPost' => $formPost->createView(),
            ]
        );
    }

    /**
     * метод редактирования поста
     * @param Request $request Request
     * @param Post    $post
     * @Route ("/edit/{post}", name="post_edit")
     * @return Response
     */
    public function edit(Request $request, Post $post, EntityManagerInterface $entityManager): Response
    {
        $formPost = $this->createForm(FeedbackPost::class, $post);
        $formPost->handleRequest($request);
        if ($formPost->isSubmitted() && $formPost->isValid()) {
            $entityManager->persist($post);
            $entityManager->flush();
            return $this->redirectToRoute(
                'post_show',
                [
                    'post' => $post->getId(),
                ]
            );
        }
        return $this->render(
            'post/edit.html.twig',
            [
            'post' => $post,
            'formPost' => $formPost->createView(),
            ]
        );
    }

    /**
     * метод удаления поста
     * @param Post $post
     * @Route ("/delete/{post}", name="post_delete")
     * @return Response
     */
    public function delete(Post $post)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        return $this->redirectToRoute('homepage');
    }

    /**
     * метод демонстрации поста
     * @param Post $post
     * @Route ("/show/{post}", name="post_show")
     * @return Response
     */
    public function showPost(Post $post): Response
    {
        return $this->render(
            'post/show.html.twig',
            [
                'post' => $post,
            ]
        );
    }


    /**
     * @Route("/download/{post}.csv", name="post_download_csv")
     *
     * @param Post            $post
     * @param PostExporterCsv $exporterHtml
     *
     * @return Response
     */
    public function downloadCvsAction(Post $post, PostExporterCsv $exporterHtml)
    {
        $exporterHtml->setPost($post);
        $content = $exporterHtml->export();

        $filename = $post->getName() . '.' . $exporterHtml->getFileExtension();

        // Return a response with a specific content
        $response = new Response($content);

        // Create the disposition of the file
        $disposition = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $filename
        );

        // Set the content disposition
        $response->headers->set('Content-Disposition', $disposition);

        // Dispatch request
        return $response;
    }

    /**
     * @Route("/download/{post}.html", name="post_download_html")
     *
     * @param Post             $post
     * @param PostExporterHtml $exporterHtml
     *
     * @return Response
     */
    public function downloadHtmlAction(Post $post, PostExporterHtml $exporterHtml)
    {
        $exporterHtml->setPost($post);
        $content = $exporterHtml->export();

        $filename = $post->getName() . '.' . $exporterHtml->getFileExtension();

        // Return a response with a specific content
        $response = new Response($content);

        // Create the disposition of the file
        $disposition = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $filename
        );

        // Set the content disposition
        $response->headers->set('Content-Disposition', $disposition);

        // Dispatch request
        return $response;
    }

    /**
     * @Route("/download/{post}", name="post_download")
     *
     * @param Post                  $post
     * @param PostExporterInterface $exporter
     *
     * @return Response
     */
    public function downloadAction(Post $post, PostExporterInterface $exporter)
    {
        $exporter->setPost($post);
        $content = $exporter->export();

        return new Response($content);
    }

}
