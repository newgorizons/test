<?php
namespace App\Form;

use phpDocumentor\Reflection\Types\Void_;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class FormPost extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options) :void
    {
            $builder->add(
                'name',
                TextType::class,
                [
                    'label' => 'Name',
                ]
            );
        $builder->add(
            'description',
            TextType::class,
            [
                'label' => 'My Desck',
            ]
        );
        $builder->add(
            'publishedAt',
            DateType::class,
            [
                'label' => 'Date of publication  ',
                'widget' => 'single_text',
            ]
        );
        $builder->add(
            'submit',
            SubmitType::class,
            [
                'attr' => [
                    'class' => 'form-control',
                ],
            ]
        );
    }
}
